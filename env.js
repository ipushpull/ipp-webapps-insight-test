const ENV = {
  ipushpull: {
    api_url: "https://test.ipushpull.com/api",
    auth_url: "https://auth-insight.ipushpull.com",    
    app_url: "https://insight-test.ipushpull.com",
    api_key: "LC8aOmGqqydL3n8HyKaz8ue34M13WPux2zyuBAl1",
    api_secret: "kXXRR7jsoEm6JgjvBDYGca1YlGIDynuxpM9kiu9GTlqzLtmCvNWLm3pmzWUc6Uptp4DwvS1kLlC6mh5PHGGnKRNh0eBPG1IIUB5NtsEY1oSgqLcteNCdGBLnIwnOO7SY",
    hsts: false,
    storage_prefix: "ipp-uat-insight-wl",
    storage_host: "ipushpull.com",
    transport: "polling",
    client_version: "",
  },
  clients: {
    ipushpull: {
      app: {
        auth: {
          ipushpull: {
            client_version: "auth",
          },
          images: [],
          theme: {
            ui: {
              show_forgotten_password: false
            },
          },
          logo: "enterprise/img/InsightLogin.png",
        },
        embed: {
          ipushpull: {
            client_version: "embed",
          },
        },
        mobile: {
          ipushpull: {
          },
          client_application: "mobile",
        },
        client: {
          ipushpull: {
            client_version: "client",
          },
          stripe: {
            key: "",
          },
          title_suffix: 'Insight Investment',
          theme: {
            upgrade: {
              version: "2021.9.2",
              type: "minor",
              release: {
                label: "Find out more",
                help_doc: "https://support.ipushpull.com",
              },
              message:
                "<p>There's a new version of the ipushpull app available, which includes:</p><ul><li>Streamlined onboarding for new and invited users</li><li>Microsoft Teams and Slack integrations coming soon - get in touch for a preview</li></ul>",
              logo: {
                light:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-new.svg",
                dark:
                  "https://ipushpull.s3.eu-west-1.amazonaws.com/static/logo-white.svg",
              },
            },
            mobile_ui: {
              nav_create: false,
              nav_help: false,
              nav_share: false,
              nav_setup: false,  
              nav_home_startup: true, 

              profile_tab_integrations: false,
              profile_tab_keys: false,

              folders_and_pages_types: [0, 8],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_tab_recent: false,
              folders_and_pages_tab_favs: false,              

              toolbar_fav: false,
              toolbar_columns: false,
              toolbar_highlights: false,
              toolbar_tracking: false,
              toolbar_filters: false,
              toolbar_sort: false,
              toolbar_share: false,              
              toolbar_menu_view_fit_height: false,
              toolbar_menu_view_fit_contain: false,    
              toolbar_menu_view_show_headings: false,
              toolbar_menu_view_show_row_selection: false,
              toolbar_menu_view_show_gridines: false,
              toolbar_menu_view_show_cell_bar: false,
              toolbar_menu_data: false,
              toolbar_menu_page: false,     
              
              grid_menu_copy: false,
              grid_menu_cell: false,                        

              user_theme_selection: false,
              workspaces_tabs_limit: 6,
              workspaces_show_views: false,
              workspaces_page_update_format: "HH:mm:ss",              
            },
            ui: {
              nav_workspaces: true,
              nav_folders: true,
              nav_create: false,
              nav_help: false,
              nav_share: false,
              nav_setup: false,
              nav_setup_lsd: true,
              nav_setup_dod: true,
              nav_setup_ddn: true,
              nav_setup_caw: true,
              nav_setup_admin: true,
              nav_home_startup: true, 
              nav_profile_download: false,

              profile_tab_integrations: false,
              profile_tab_keys: false,

              folders_and_pages_types: [0, 8],
              folders_and_pages_can_pin_folder: true,
              folders_and_pages_can_leave_folder: true,
              folders_and_pages_tab_recent: false,
              folders_and_pages_tab_favs: false,

              toolbar: true,
              toolbar_fav: false,
              toolbar_autosave: true,
              toolbar_columns: false,
              toolbar_highlights: false,
              toolbar_tracking: false,
              toolbar_filters: false,
              toolbar_sort: false,
              toolbar_views: true,
              toolbar_share: false,

              toolbar_menu_view: true,
              toolbar_menu_view_fit: true,
              toolbar_menu_view_fit_scroll: true,
              toolbar_menu_view_fit_width: true,
              toolbar_menu_view_fit_height: false,
              toolbar_menu_view_fit_contain: false,

              toolbar_menu_view_show: true,
              toolbar_menu_view_show_headings: false,
              toolbar_menu_view_show_row_selection: false,
              toolbar_menu_view_show_row_highlight: true,
              toolbar_menu_view_show_gridines: false,
              toolbar_menu_view_show_cell_bar: false,
              toolbar_menu_view_show_filters: true,
              toolbar_menu_view_freeze: false,

              toolbar_menu_data: false,

              toolbar_menu_page: false,

              page_views: true,

              grid_menu_copy: false,
              grid_menu_filter: true,
              grid_menu_sort: true,
              grid_menu_freeze: true,
              grid_menu_rowscols: true,
              grid_menu_cell: false,
              grid_menu_share: false,
              
              workspaces_icon_full_screen: false,
              workspaces_toolbar: false,
              workspaces_show_views: true,
              workspaces_page_update_format: "HH:mm:ss",
              workspaces_tab_linking: true,

              user_theme_selection: false,
            },
            logo: {
              light: "enterprise/img/insight-logo.png",
              dark: "enterprise/img/insight-logo.png",
            },
            color: {
              nav: {
                button_class: "ipp-theme-dark text-white",
                background_class: "ipp-theme-dark",
                style: { "background-color": "#005432" }
              }
            }            
          },
          data_sources: [
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
                  iframe: "https://test.ipushpull.com/help.php?id=647790593",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "Database",
              key: "db",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/db.svg",
              // help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
                  iframe: "https://test.ipushpull.com/help.php?id=148865025",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: -1,
              name: "API",
              key: "api",
              logo: "",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/xsgmc85x",
                  iframe: "https://test.ipushpull.com/help.php?id=681607177",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
          ],
          client_apps: [
            {
              id: 2,
              name: "Desktop App",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                  iframe: "https://test.ipushpull.com/help.php?id=681705505",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
              appType: ["lsd"],
            },
            {
              id: 41,
              name: "Mobile App",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/l/c/qxv9HZC1",
                  iframe: "https://test.ipushpull.com/help.php?id=650248196",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
                  iframe: "https://test.ipushpull.com/help.php?id=156074055",
                },
                dod: {
                  desk: "",
                  iframe: "",
                },
              },
            },
            {
              id: 38,
              name: "Microsoft Teams App",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 42,
              name: "Microsoft Teams Bot",
              key: "teamsBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
                  iframe: "https://test.ipushpull.com/help.php?id=678494209",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/aut0u3UB",
                  iframe: "https://test.ipushpull.com/help.php?id=741670913",
                },
              },
            },
            {
              id: 7,
              name: "Slack Bot",
              key: "slack",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/slack.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "",
                  iframe: "",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/X5EwR7Dy",
                  iframe: "https://test.ipushpull.com/help.php?id=156598336",
                },
              },
            },
            {
              id: 14,
              name: "Symphony App",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["lsd"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },
            {
              id: 43,
              name: "Symphony Bot",
              key: "symphonyBot",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              appType: ["dod"],
              help: {
                lsd: {
                  desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
                  iframe: "https://test.ipushpull.com/help.php?id=695959587",
                },
                dod: {
                  desk: "https://ipushpull.atlassian.net/l/c/DeKPs5es",
                  iframe: "https://test.ipushpull.com/help.php?id=742162433",
                },
              },
            },

            // {
            //   id: -1,
            //   name: "WordPress",
            //   key: "wp",
            //   logo: {
            //     light:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp.svg",
            //     dark:
            //       "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/wp-white.svg",
            //   },
            //   help_doc: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
            // },
          ],
          lsd_apps: [
            {
              id: 2,
              name: "Desktop app",
              key: "ipp",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
              help: [
                {
                  title: "Client App overview",
                  url: "https://ipushpull.atlassian.net/l/c/PHy2bZn0",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/7j4QXJHj",
                },
              ],
            },
            {
              id: 41,
              name: "Mobile app",
              key: "mobile",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/ipp.svg",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
            },
            {
              id: 3,
              name: "Excel Add-in",
              key: "excel",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/excel.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
              help: [
                {
                  title: "Install the Excel Add-in",
                  url: "https://ipushpull.atlassian.net/l/c/NXJ1Js3d",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/WHeWjzmk",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/L9oPttf9",
                },
              ],
            },
            {
              id: 38,
              name: "Microsoft Teams",
              key: "teams",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/teams.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
              help: [
                {
                  title: "Install the Teams app",
                  url: "https://ipushpull.atlassian.net/l/c/TuecR7Ar",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/l/c/U0CeB7fZ",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/sZorb7Kt",
                },
              ],
            },
            {
              id: 14,
              name: "Symphony",
              key: "symphony",
              logo:
                "https://ipushpull.s3.eu-west-1.amazonaws.com/website/icons/symphony.png",
              help_doc: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
              help: [
                {
                  title: "Symphony app overview",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Pull Live & Streaming Data",
                  url: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
                },
                {
                  title: "Find out more ...",
                  url: "https://ipushpull.atlassian.net/l/c/VtU22Dzj",
                },
              ],
            },
          ],
        },
      },
    },
  },
  sso_only: false,
  sso_url: "",
  on_prem: true,
  billing: false,
  help_docs: {
    livestreaming_data_source_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYCcJg",
      iframe: "https://test.ipushpull.com/help.php?id=647790593",
    },
    livestreaming_data_source_db: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDfC",
      iframe: "https://test.ipushpull.com/help.php?id=148865025",
    },
    livestreaming_data_source_api: {
      desk: "https://ipushpull.atlassian.net/wiki/x/CYCgK",
      iframe: "https://test.ipushpull.com/help.php?id=681607177",
    },
    livestreaming_client_app_ipp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/IQCiK",
      iframe: "https://test.ipushpull.com/help.php?id=681705505",
    },
    livestreaming_client_app_excel: {
      desk: "https://ipushpull.atlassian.net/wiki/x/R4BNCQ",
      iframe: "https://test.ipushpull.com/help.php?id=156074055",
    },
    livestreaming_client_app_symphony: {
      desk: "https://ipushpull.atlassian.net/wiki/x/I4B7KQ",
      iframe: "https://test.ipushpull.com/help.php?id=695959587",
    },
    livestreaming_client_app_wp: {
      desk: "https://ipushpull.atlassian.net/wiki/x/a4CBKQ",
      iframe: "https://test.ipushpull.com/help.php?id=696352875",
    },
    livestreaming_client_app_teams: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AQBxK",
      iframe: "https://test.ipushpull.com/help.php?id=678494209",
    },
    notifications_builder_excel_data_source: {
      desk: "https://ipushpull.atlassian.net/wiki/x/AYDmI",
      iframe: "https://test.ipushpull.com/help.php?id=551976961",
    },
    notifications_builder_msteams_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/H4BeHg",
      iframe: "https://test.ipushpull.com/help.php?id=509509663",
    },
    notifications_builder_slack_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/JgBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542438",
    },
    notifications_builder_symphony_webhook: {
      desk: "https://ipushpull.atlassian.net/wiki/x/LwBfHg",
      iframe: "https://test.ipushpull.com/help.php?id=509542447",
    },
    symphony_overview: {
      desk: "https://ipushpull.atlassian.net/wiki/x/UYCHDQ",
      iframe: "https://test.ipushpull.com/help.php?id=226984017",
    },
    user_authoriziations: {
      desk: "https://ipushpull.atlassian.net/l/c/GApuWLAA",
      iframe: "https://test.ipushpull.com/help.php?id=631701505",
    },
  },
};
